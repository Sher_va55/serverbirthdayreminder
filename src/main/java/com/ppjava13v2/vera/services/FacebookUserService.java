package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.*;
import com.ppjava13v2.vera.db.DAO.FacebookUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.*;
import java.util.Date;


@Service
public class FacebookUserService {

    @Autowired
    private FacebookUserDAO facebookUserDAO;


    public FacebookUser findByUserId (long userId) {
        return facebookUserDAO.findByUserId(userId);
    }

    public FacebookUser  findByFacebookId (String fbId) {
        return facebookUserDAO.findByFacebookId(fbId);
    }


    public boolean saveFBUser(long user_id, String fbFIO, String fbID, String birthday) {

        User user= new User (user_id);
        Date birthdayDate;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

        try {

            birthdayDate = formatter.parse(birthday);
            FacebookUser facebookUser = new FacebookUser(user,fbFIO,fbID,birthdayDate);

            if (facebookUserDAO.saveAndFlush(facebookUser)!=null) {
                return true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

}