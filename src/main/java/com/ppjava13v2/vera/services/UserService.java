package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.User;

import java.util.Collection;

/**
 * Interface provides methods for retrieving the User by his id, login, list all the users and create a new one
 */
public interface UserService {

    User getUserById(long id);

    User getUserByLogin(String login);

    Collection<User> getAllUsers();

    boolean create(String login,String password);

    boolean updateGCMToken (long id,String GCMToken);

    boolean updateFacebookToken (long id,String facebookToken);

    boolean updateTwitterToken (long id,String twitterToken);
}