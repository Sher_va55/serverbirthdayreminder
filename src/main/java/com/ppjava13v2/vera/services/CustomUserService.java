package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.*;
import com.ppjava13v2.vera.db.DAO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.text.*;
import java.util.Date;


@Service
public class CustomUserService {

    @Autowired
    CustomUserDAO customUserDAO;

    @Autowired
    FriendsListDAO friendsListDAO;

    @Autowired
    UserServiceImpl userServiceImpl;

    public CustomUser findByUserId (long userId) {

        return customUserDAO.findByUserId(userId);
    }

    public boolean saveCustomUser (String FIO,String birthday,User mappedUser) {

        User user= new User ();
        long lastId=userServiceImpl.findMaxId();
        user.setLogin("custom"+(++lastId));
        user.setPasswordHash("custom");
        user.setRole(Role.USER);

        Date birthdayDate;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        try {
            birthdayDate = formatter.parse(birthday);
            CustomUser customUser = new CustomUser (user,FIO,birthdayDate);

            customUser=customUserDAO.saveAndFlush(customUser);

            FriendsList friendsList=new FriendsList(mappedUser,customUser.getUser());

            if (friendsListDAO.saveAndFlush(friendsList)!=null) {
                return true;
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }
}
