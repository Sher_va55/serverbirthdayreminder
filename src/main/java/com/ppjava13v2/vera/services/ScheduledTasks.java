package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.*;
import com.ppjava13v2.vera.services.GCMServices.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;


@Component
public class ScheduledTasks {

    @Autowired
    private CreateContentService content;

    @Autowired
    private FriendsListService friendsListService;

    @Autowired
    private FacebookUserService facebookUserService;

    @Autowired
    CustomUserService customUserService;


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");


    /**
     * Sends push notification at 12-30  every day
     */
    @Scheduled (cron = "0 47 22 * * *")
    public void sendRemindNotification () {


        List<FriendsList> socialFriends= friendsListService.selectCurrentBirthday();

        for (FriendsList socialFriend:socialFriends) {

            User friend=socialFriend.getFriend();
            User user=socialFriend.getUser();
            content.setTo(user.getGcmToken());

            FacebookUser friendFacebookUser=facebookUserService.findByUserId(friend.getId());
            CustomUser customUser=customUserService.findByUserId(friend.getId());

            if (friendFacebookUser!=null) {
                content.createData("Remind","У вашего друга facebook  "+ friendFacebookUser.getFIO() + " сегодня день рождения");
                PostGCMService.post(content);
            }

            else if (customUser!=null) {

                content.createData("Remind","У вашего друга  "+ customUser.getFIO() + " сегодня день рождения");
                PostGCMService.post(content);
            }

        }


        System.out.println("The time is now " + dateFormat.format(new Date()));

    }

}


