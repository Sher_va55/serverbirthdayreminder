package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.CurrentUser;
import com.ppjava13v2.vera.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Implementation of UserDetailsService interface.
 * Interface the Spring Security uses to find out if the user using the login form exists,
 * what their password should be, and what authority it has in the system
 */

@Service
public class CurrentUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Autowired
    public CurrentUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @param login user's login
     * @return UserDetails instance if the username exists
     * @throws UsernameNotFoundException
     */
    @Override
    public CurrentUser loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userService.getUserByLogin(login);
        return new CurrentUser(user);
    }
}