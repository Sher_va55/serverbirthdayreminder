package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.*;
import com.ppjava13v2.vera.db.DAO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Provides methods for operations with user's friends
 */
@Service
public class FriendsListService {

    @Autowired
    FriendsListDAO friendsListDAO;

    public List<FriendsList> findFriendsByUserId (long id) {
        return friendsListDAO.findByUserId(id);
    }

    public List<FriendsList> findUsersByFriendId (long id) {
        return friendsListDAO.findByFriendId(id);
    }

    public FriendsList findByUserIdAndFriendId (long userId,long friendId) {

        return friendsListDAO.findByUserIdAndFriendId(userId,friendId);
    }

    public boolean saveFriends (long user_id, long friend_id) {

        User user=new User (user_id);
        User friend=new User (friend_id);

        FriendsList facebookFriends=new FriendsList(user,friend);

        if (friendsListDAO.saveAndFlush(facebookFriends)!=null)
            return true;

        return false;
    }

    public List <FriendsList> selectCurrentBirthday() {
        return friendsListDAO.selectCurrentBirhday();
    }
}
