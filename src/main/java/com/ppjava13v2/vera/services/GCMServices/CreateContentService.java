package com.ppjava13v2.vera.services.GCMServices;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Holds POST body content that will be converted to JSON when sending the request to GCM server
 */
@Service
public class CreateContentService implements Serializable {

    @JsonProperty("to")
    private String to;

    @JsonProperty("data")
    private Map<String, String> data;

    public void setTo(String to) {
        this.to = to;
    }



    public void createData(String title, String message) {
        if (data == null)
            data = new HashMap<String, String>();

        data.put("title", title);
        data.put("message", message);
    }
}

