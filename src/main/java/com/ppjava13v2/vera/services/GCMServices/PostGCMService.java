package com.ppjava13v2.vera.services.GCMServices;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Sends post request to GCM server
 */
@Service
public class PostGCMService {

    public static final String apiKey="";

    public static void post (CreateContentService content) {

        URL url;

        try {

            url = new URL("https://gcm-http.googleapis.com/gcm/send");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "key="+apiKey);


            ObjectMapper mapper = new ObjectMapper();

            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());

            mapper.writeValue(wr, content);

            wr.flush();
            wr.close();

            int responseCode = connection.getResponseCode();
            System.out.println("Response Code : " + responseCode);


            if (responseCode==200) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String response = reader.readLine();
                reader.close();
                System.out.println(response);
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
