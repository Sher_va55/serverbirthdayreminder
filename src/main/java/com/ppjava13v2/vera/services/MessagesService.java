package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.Messages;
import com.ppjava13v2.vera.db.DAO.MessagesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Provides methods for retrieving the message by his id and saving a new one
 */
@Service
public class MessagesService {

    @Autowired
    private MessagesDAO messagesDAO;

    public Messages findMessageById (long id) {
        return  messagesDAO.findOneById(id);
    }

    public boolean saveMessage (String text) {

        Messages message=new Messages(text);

        if (messagesDAO.saveAndFlush (message)!=null)
            return true;

        return false;
    }
}
