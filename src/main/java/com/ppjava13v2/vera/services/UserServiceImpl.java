package com.ppjava13v2.vera.services;

import com.ppjava13v2.vera.Entities.*;
import com.ppjava13v2.vera.db.DAO.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Implementation UserService that provides methods for retrieving the User by his id, login, list all the users and create a new one
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private   UserDAO userDAO;

    @Override
    public User getUserById(long id) {
        return userDAO.findOne(id);
    }

    @Override
    public User getUserByLogin(String login) {
        return userDAO.findOneByLogin(login);
    }

    @Override
    public Collection<User> getAllUsers() {
        return userDAO.findAll();
    }

    @Override
    public boolean create(String login, String password) {
        User user = new User();


        if (getUserByLogin (login)==null) {
            user.setLogin(login);
            user.setPasswordHash(new BCryptPasswordEncoder().encode(password));
            user.setRole(Role.USER);
            userDAO.save(user);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean updateGCMToken (long id,String GCMToken) {
        userDAO.updateGCMToken(GCMToken,id);
        return true;
    }

    @Override
    public boolean updateFacebookToken (long id,String facebookToken) {
        userDAO.updatefacebookToken(facebookToken,id);
        return true;
    }

    @Override
    public boolean updateTwitterToken (long id,String twitterToken) {
        userDAO.updatetwitterToken(twitterToken,id);
        return true;
    }

    public long findMaxId () {

        return userDAO.findMaxId();
    }

}