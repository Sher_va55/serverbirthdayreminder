package com.ppjava13v2.vera.db.DAO;

import com.ppjava13v2.vera.Entities.*;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Provides operations with messages in database
 */
public interface MessagesDAO extends JpaRepository<Messages, Long> {

    Messages findOneById(long id);
}
