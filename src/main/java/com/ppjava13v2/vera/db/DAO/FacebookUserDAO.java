package com.ppjava13v2.vera.db.DAO;


import com.ppjava13v2.vera.Entities.FacebookUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FacebookUserDAO extends JpaRepository <FacebookUser,Long> {

    FacebookUser findById(long id);
    List<FacebookUser> findAll ();
    FacebookUser findByFacebookId (String facebookId);
    FacebookUser findByUserId (long id);
}
