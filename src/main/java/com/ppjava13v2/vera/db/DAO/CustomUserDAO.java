package com.ppjava13v2.vera.db.DAO;

import com.ppjava13v2.vera.Entities.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Provides operations with custom users data in database
 */
public interface CustomUserDAO extends JpaRepository <CustomUser,Long> {

    CustomUser findById(long id);
    CustomUser findByUserId (long id);

}
