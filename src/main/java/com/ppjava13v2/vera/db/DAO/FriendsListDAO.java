package com.ppjava13v2.vera.db.DAO;

import com.ppjava13v2.vera.Entities.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Provides operations with users and their friends in database
 */
public interface FriendsListDAO extends JpaRepository<FriendsList,Long> {

    FriendsList findOneById(long id);
    List<FriendsList>findByUserId (long id);
    List<FriendsList>findByFriendId (long id);
    FriendsList findByUserIdAndFriendId (long userId,long friendId);
    FriendsList findByMessagesId (long id);


    @Query("select a from FriendsList a where a.friend in "+
    "(select b.user  from FacebookUser b where Month (b.birthday) =Month (CURRENT_DATE) "+
            "and day (b.birthday)=day (CURRENT_DATE))"+
    "or a.friend in (select c.user  from CustomUser c where Month (c.birthday) =Month (CURRENT_DATE) " +
            "and day (c.birthday)=day (CURRENT_DATE))")
    List <FriendsList> selectCurrentBirhday ();

}
