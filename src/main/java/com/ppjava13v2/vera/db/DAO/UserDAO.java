package com.ppjava13v2.vera.db.DAO;

import com.ppjava13v2.vera.Entities.FriendsList;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ppjava13v2.vera.Entities.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * Provides operations with users in database
 */
public interface UserDAO extends JpaRepository<User, Long> {

    User findOneByLogin(String login);

    @Query("select max (a.id) from User a")
    long findMaxId ();

    @Transactional
    @Modifying
    @Query("update User u set u.gcmToken = :gcmToken where u.id = :id")
    void updateGCMToken(@Param("gcmToken")String gcmToken, @Param("id")long id);

    @Transactional
    @Modifying
    @Query("update User u set u.facebookToken = :facebookToken where u.id = :id")
    void updatefacebookToken(@Param("facebookToken")String facebookToken, @Param("id")long id);


    @Transactional
    @Modifying
    @Query("update User u set u.twitterToken = :twitterToken where u.id = :id")
    void updatetwitterToken(@Param("twitterToken")String twitterToken, @Param("id")long id);
}