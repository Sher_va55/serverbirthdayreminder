package com.ppjava13v2.vera.controllers;

import com.ppjava13v2.vera.Entities.*;
import com.ppjava13v2.vera.services.*;
import com.ppjava13v2.vera.services.GCMServices.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.facebook.api.*;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@Controller
public class FacebookFriendsController {

    @Autowired
    UserService userService;

    @Autowired
    CreateContentService content;

    @Autowired
    FacebookUserService facebookUserService;

    @Autowired
    FriendsListService friendsListService;

    @Autowired
    CustomUserService customUserService;


    @RequestMapping(value="/auth", method= RequestMethod.POST)
    @ResponseBody
    public String createUser (@RequestParam String login,@RequestParam String password) {

        boolean result=userService.create(login,password);
        if (!result)
            throw new IllegalArgumentException("The login has already exists");
        return "success";
    }


    @RequestMapping(value="/facebook/token", method= RequestMethod.POST)
    public String saveFacebookToken(@RequestParam String accessToken) {

        CurrentUser currentUser =(CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        com.ppjava13v2.vera.Entities.User user = currentUser.getUser();
        long userId=user.getId();

        userService.updateFacebookToken(userId, accessToken);
        Facebook facebook=new FacebookTemplate(accessToken);
        User profile = facebook.fetchObject("me", User.class, "id", "name", "link", "birthday");

        String test=profile.getBirthday();

        if (facebookUserService.findByUserId(userId)==null)
            facebookUserService.saveFBUser(userId,profile.getName(),profile.getId(),profile.getBirthday());

         return "redirect:/facebook/save/friends";
    }

    @RequestMapping(value="/facebook/save/friends", method= RequestMethod.GET)
    @ResponseBody
    public String saveFriends() {

        CurrentUser currentUser =(CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        com.ppjava13v2.vera.Entities.User user = currentUser.getUser();

        Facebook facebook=new FacebookTemplate(user.getFacebookToken());
        List<User> friends = facebook.friendOperations().getFriendProfiles();

        for (User friend:friends) {

            String fbId=friend.getId();
            FacebookUser facebookUser=facebookUserService.findByFacebookId (fbId);

            if (null!=facebookUser) {
                long friendId=facebookUser.getUser().getId();

                if (null== friendsListService.findByUserIdAndFriendId(user.getId(),friendId)) {
                    friendsListService.saveFriends(user.getId(), friendId);
                }
            }
        }

        return "success";

    }

    @RequestMapping(value="/facebook/get/friends", method= RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public List<SocialUser> getFriends() {

        CurrentUser currentUser =(CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        com.ppjava13v2.vera.Entities.User user = currentUser.getUser();

        List<FriendsList> facebookFriends= friendsListService.findFriendsByUserId(user.getId());
        List <SocialUser> friends= new ArrayList<SocialUser>();

        for (FriendsList facebookFriend:facebookFriends) {

            FacebookUser fbUser=facebookUserService.findByUserId(facebookFriend.getFriend().getId());
            if (fbUser!=null) {
                friends.add (fbUser);
            }

            CustomUser customUser=customUserService.findByUserId(facebookFriend.getFriend().getId());

            if (customUser!=null) {
                friends.add (customUser);
            }

        }

        return friends;

    }

    @RequestMapping(value="/client/save/custom", method= RequestMethod.POST)
    @ResponseBody
    public String saveCustomUser(@RequestParam String FIO,@RequestParam String birthday) {

        CurrentUser currentUser =(CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        com.ppjava13v2.vera.Entities.User user = currentUser.getUser();

        customUserService.saveCustomUser(FIO,birthday,user);

        return "success";
    }



    @RequestMapping(value="/GCM/saveToken", method= RequestMethod.POST)
    @ResponseBody
    public String saveGCMToken (@RequestParam String token) {

        CurrentUser currentUser =(CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        com.ppjava13v2.vera.Entities.User user = currentUser.getUser();
        long id = user.getId();
        String login = user.getLogin();

        userService.updateGCMToken(id,token);

        return "success";

    }
}