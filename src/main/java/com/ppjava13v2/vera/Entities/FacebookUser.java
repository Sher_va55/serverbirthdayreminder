package com.ppjava13v2.vera.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@JsonIgnoreProperties({"user"})
@Entity
public class FacebookUser implements Serializable,SocialUser {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    User user;

    private String FIO;
    private String facebookId;
    private Date birthday;

    public FacebookUser() {
    }

    public FacebookUser(User user, String facebookFIO, String facebookId, Date birthday) {
        this.user = user;
        this.FIO = facebookFIO;
        this.facebookId = facebookId;
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String facebookFIO) {
        this.FIO = facebookFIO;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}