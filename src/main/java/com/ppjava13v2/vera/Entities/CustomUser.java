package com.ppjava13v2.vera.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@JsonIgnoreProperties({"user"})
@Entity
public class CustomUser implements Serializable,SocialUser {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne (cascade = {CascadeType.ALL})
    User user;

    private String FIO;
    private String customId;
    private Date birthday;

    public CustomUser() {
    }

    public CustomUser(User user, String FIO, String customId, Date birthday) {
        this.user = user;
        this.FIO = FIO;
        this.customId = customId;
        this.birthday = birthday;
    }

    public CustomUser(User user, String FIO, Date birthday) {
        this.user = user;
        this.FIO = FIO;
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
