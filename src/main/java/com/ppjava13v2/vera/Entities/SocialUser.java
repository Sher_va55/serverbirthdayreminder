package com.ppjava13v2.vera.Entities;

import java.util.Date;

public interface SocialUser {

    public long getId();
    public void setId(long id);
    public User getUser();
    public void setUser(User user);
    public void setFIO(String FIO);
    public String getFIO();
    public Date getBirthday();
    public void setBirthday(Date birthday);
}
