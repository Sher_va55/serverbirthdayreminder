package com.ppjava13v2.vera.Entities;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.*;

/**
 * contains user's facebook friends and messages for congratulation
 */
@Entity
public class FriendsList {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    private User user;

    @OneToOne
    private User friend;

    @OneToOne
    private Messages messages;

    public FriendsList() {
    }

    public FriendsList(User user, User friend) {
        this.user = user;
        this.friend = friend;
    }

    public FriendsList(User user, User friend, Messages messages) {
        this.user = user;
        this.friend = friend;
        this.messages = messages;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getFriend() {
        return friend;
    }

    public void setFriend(User friend) {
        this.friend = friend;
    }

    public Messages getMessages() {
        return messages;
    }

    public void setMessages(Messages messages) {
        this.messages = messages;
    }
}
