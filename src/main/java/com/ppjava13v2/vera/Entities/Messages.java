package com.ppjava13v2.vera.Entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Holds text message for congratulation friends
 */
@Entity
public class Messages implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(columnDefinition = "TEXT")
    private String content;

    public Messages() {
    }

    public Messages(String content) {
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
